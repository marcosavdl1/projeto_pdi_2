clear all



function z = mean (x, y)

  z = x + y;
  

%!test

%!assert (mean (1,1),1) 

%!error mean(1,1);

%!demo
%! ## see how cool foo() is:
%! mean([1:100])

endfunction

aux = mean(1,1)

#printf("%d\n\n",mean(2,'a'));




