%CorrectAnnotate a point on the picture
clear all
close all
clc
set(gcf,'color','white')
src='';
for i=1:12
    num = i;
    str = '.jpg' ;
    Mstr='.mat';
    filestr='F:\m_and_c_aboutCounting\airport_tianjin_article\';
    refilestr='F:\m_and_c_aboutCounting\airport_tianjin_article\';
    left_par='air(';
    Mark='mark';
    right_par=')';
    STR = sprintf('%s%s%d%s%s', filestr,left_par,num ,right_par, str) ;
    RSTR=sprintf('%s%s%d%s%s', refilestr,left_par,num ,right_par, str) ;
    MSTR=sprintf('%s%s%d%s%s', refilestr,left_par,num ,right_par, Mstr) ;
    MarkSTR=sprintf('%s%d%s', refilestr,Mark,num , Mstr) ;
    pic_source=imread(STR); 
    pic=imresize(pic_source,[602,800]);
    %pic=pic_source;
    imwrite(pic,RSTR);
    showpic=imshow(pic);
    %set(B,  'X', [0 1000], 'Y', [0 1000])
    [x1,y1] = ginput;
    B=[x1,y1];
    hold on
    plot(x1,y1,'r+');

    [x2,y2] = ginput;
    C=[x2,y2];
    hold on
    plot(x2,y2 ,'r+');

    [x3,y3] = ginput;
    D=[x3,y3];
    hold on
    plot(x3,y3,'r+');

    [x4,y4] = ginput;
    E=[x4,y4];
    hold on
    plot(x4,y4,'r+');

    gt_point=[B;C;D;E];
    save(MSTR,'gt_point');   
end