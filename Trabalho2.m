
VideoSize = [432 528];

filename = "/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/videoplayback.mp4";
hvfr = VideoReader(filename);



% Acknowledgement
ackText = ['Data set courtesy of Jonathan Young and Michael Elowitz, ' ...
             'California Institute of Technology'];

 
hVideo = vision.VideoPlayer;
hVideo.Name  = 'Results';
hVideo.Position(1) = round(hVideo.Position(1));
hVideo.Position(2) = round(hVideo.Position(2));
hVideo.Position([4 3]) = 30+VideoSize;



frameCount = int16(1);
while hasFrame(hvfr)
    % Read input video frame
    image = im2gray(im2single(readFrame(hvfr)));

    % Apply a combination of morphological dilation and image arithmetic
    % operations to remove uneven illumination and to emphasize the
    % boundaries between the cells.
    y1 = 2*image - imdilate(image, strel('square',7));
    y1(y1<0) = 0;
    y1(y1>1) = 1;
    y2 = imdilate(y1, strel('square',7)) - y1;

    th = multithresh(y2);      % Determine threshold using Otsu's method    
    y3 = (y2 <= th*0.7);       % Binarize the image.
    
    Centroid = step(hblob, y3);   % Calculate the centroid
    numBlobs = size(Centroid,1);  % and number of cells.
    % Display the number of frames and cells.
    frameBlobTxt = sprintf('Frame %d, Count %d', frameCount, numBlobs);
    image = insertText(image, [1 1], frameBlobTxt, ...
            'FontSize', 16, 'BoxOpacity', 0, 'TextColor', 'white');
    image = insertText(image, [1 size(image,1)], ackText, ...
            'FontSize', 10, 'AnchorPoint', 'LeftBottom', ...
            'BoxOpacity', 0, 'TextColor', 'white');

    % Display video
    image_out = insertMarker(image, Centroid, '*', 'Color', 'green');   
    step(hVideo, image_out);

    frameCount = frameCount + 1;
end



















img = "/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/teste.png";
 ## Find boundaries
bounds = bwboundaries (img);

 ## Plot result
imshow (img);
hold on
for k = 1:numel (bounds)
 plot (bounds {k} (:, 2), bounds {k} (:, 1), 'r', 'linewidth', 2);
endfor
hold off






img = imread("/home/mar~cos/DRIVE/ESTUDOS/UNIOESTE/PDI/marcos.png");
imshow (img);
#img = rgb2gray(img);
#img =  im2bw(img, graythresh(img));
#figure, imshow(img);
fill_bw1 = imfill(img,26,'holes');
figure, imshow(fill_bw1);

large_bw1 = bwareaopen(fill_bw1, 195);
labels1 = bwlabel(large_bw1);
figure, imshow(labels1);











A = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/celula.jpg");
imshow (A);
J = mmgradm (A);
figure,imshow (J);
J = rgb2gray(J);
J = histeq(J);
J = imadjust(J);
#printf("\n%d",J);
figure, imshow(J);





I = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/celula.jpg");
I = rgb2gray(I);
figure, imshow (I);
title ("Original image");
psf = fspecial ("motion", 30, 15);
blurred = imfilter (I, psf, "conv");
figure, imshow (blurred);
title ("Image with added motion blur");
var_noise = 0.00005;
blurred_noisy = imnoise (blurred, "gaussian", 0, var_noise);
figure, imshow (blurred_noisy);
title ("Image with motion blur and added Gaussian noise");
estimated_nsr = var_noise / (var(blurred_noisy(:)) - var_noise);
J = deconvwnr (blurred_noisy, psf, estimated_nsr);
figure, imshow (J)
title ({"restored image after Wiener deconvolution",
         "with known PSF and estimated NSR"});





S = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/moedas-299581-article.jpg");
#S = rgb2gray(S);
I = uint8 (S);
figure; imshow (I);
title ("Original penny image");
h = imhist (I);
t = otsuthresh (h);
J = im2bw (I);
figure; imshow (J);
title_line = sprintf ("Black and white penny image after thresholding, t=%g",
                     t*255);
title (title_line);
I = 255 - I;
figure; imshow(I);
title ("Negative penny image");
h = imhist (I);
t = otsuthresh (h);
J = im2bw (I);
figure; imshow (J);
title_line = sprintf ("Black and white negative penny image after thresholding, t=%g",
                     t*255);
title (title_line);






S = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/2021-04-05_558x532_scrot.png");
S = rgb2gray(S); 
figure, imshow (S);
P = imclearborder(S);
figure, imshow (P);
P = im2bw(S,graythresh (S,"Otsu"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"concavity"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"intermodes"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"intermeans"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"MaxEntropy"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"MaxLikelihood"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"mean"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"MinError"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"minimum"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"moments"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);
P = im2bw(S,graythresh (S,"percentile"));
figure, imshow (P);
P = imclearborder(P);
figure, imshow (P);



A = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-1/2021-04-05_558x532_scrot.png");
I = rgb2gray(A);

#I = A;
I = histeq(I);
J = imadjust(I);
#figure,imshow (J);
I = imadjust (I);
#I = imclearborder(I);
I = wiener2(I, [5 5]);
bw = im2bw(I, graythresh(I,"intermeans"));
figure,imshow (bw);
bw2 = imfill(bw,'holes');
bw3 = imopen(bw2, strel('disk',1,0));
bw4 = bwareaopen(bw3, 5);
figure,imshow (bw4);
maxs = imextendedmax(I,0.007);
figure,imshow (maxs);
maxs = imclose(maxs, strel('disk',1,0));
figure,imshow (maxs);
maxs = imfill(maxs, 'holes');
figure,imshow (maxs);
maxs = bwareaopen(maxs, 1);
figure,imshow (maxs);
Jc = imcomplement(I);
figure,imshow (Jc);
I_mod = imimposemin(Jc, ~bw4 | maxs);
figure,imshow (I_mod);
L = watershed(I_mod);
figure,imshow (L);
labeledImage = label2rgb(L);

figure,imshow (labeledImage);


#whatershed

RGBT = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-3/1173_RGB.jpg");
A = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-3/1173_T.jpg");
I = rgb2gray(A);
figure,imshow(A);

#I = A;
RGBT = rgb2gray(A);
RGBT = histeq(RGBT);
RGBT = imadjust(RGBT);
figure,imshow (RGBT);
RGBT = imadjust (RGBT);
RGBT = imclearborder(RGBT);
RGBT = wiener2(RGBT, [5 5]);
figure, imshow(RGBT);

I = histeq(I);
J = imadjust(I);
figure,imshow (J);
I = imadjust (I);
I = imclearborder(I);
I = wiener2(I, [5 5]);
bw = im2bw(I, graythresh(I,"intermeans"));
figure,imshow (bw);
bw2 = imfill(bw,'holes');
bw3 = imopen(bw2, strel('disk',1,0));
bw4 = bwareaopen(bw3, 1);
figure,imshow (bw4);
maxs = imextendedmax(I,0.07);
figure,imshow (maxs);
maxs = imclose(maxs, strel('disk',1,0));
figure,imshow (maxs);
maxs = imfill(maxs, 'holes');
figure,imshow (maxs);
maxs = bwareaopen(maxs, 5);
figure,imshow (maxs);
Jc = imcomplement(RGBT);
#Jc = imfill(Jc,"holes");
figure,imshow (Jc);
I_mod = imimposemin(Jc, ~bw4 | maxs);
figure,imshow (I_mod);
L = watershed(I_mod);
figure,imshow (L);
labeledImage = label2rgb(L);

figure,imshow (labeledImage);

img = im2bw(img-imopen(img, strel(va3,round(va2),0)),va1);
figure,imshow (labeledImage);






#img = "/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-3/1836_RGB.jpg";
r1 = 1;
r2 = 5;

    img1 = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-3/1836_RGB.jpg");                       %read the image
    img=im2bw(img1,graythresh(img1));         %convert into binary
    
    imgBW = edge(img,"Canny");                        %'canny' edge detection
    imshow(img);
    count=0;
    FDetect = vision.CascadeObjectDetector;    %for face detection
    BB = step(FDetect,img1);                   %bounding box for face detect

    for i=r1:r2
        [ydetect,xdetect,Accumulator] = houghcircle(imgBW,i,(i*pi)); %circular hough
        y{count+1}=ydetect;                     %storing y co-ordinates
        x{count+1}=xdetect;                     %storing x co-ordinates
        count=count+1;
        i=i+1;
    end

    disp(count);
    figure;
    imshow(img1);                          %for visualizing detected heads
    hold on;
    crowd=0;
    for j=1:count
        crowd=crowd+length(x{j});          %for counting detected heads
        plot(x{j},y{j},'.','LineWidth',2,'Color','red');
        j=j+1;
    end

    disp('Number of heads:');
    disp(crowd);
    disp('Number of faces:');
    disp(size(BB,1));
    disp('Total');
    disp(crowd+size(BB,1));
###################################################################

% Read sample image
I = imread("/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-1/coins.png");
#imshow(I);
% Threshold
th = 100;
% Blobs where I > th
BW = I > th;
% Count number of blobs
s = regionprops(BW);
numBlob = numel(s);
printf("%d",numBlob)
##############################################################

#url = 'https://blogs.mathworks.com/images/steve/2013/blobs.png';
url = "/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-1/cluster1.jpg";
bw = imread(url);
bw = im2bw(bw);
figure,imshow(bw);
L = watershed(bw);
Lrgb = label2rgb(L);
figure,imshow(Lrgb);

bw2 = ~bwareaopen(~bw, 10);
figure,imshow(bw2);

D = -bwdist(~bw);
figure,imshow(D,[])

Ld = watershed(D);
figure,imshow(label2rgb(Ld))

bw2 = bw;
bw2(Ld == 0) = 0;
figure,imshow(bw2)

mask = imextendedmin(D,2);
#imshowpair(bw,mask,'blend')

D2 = imimposemin(D,mask);
Ld2 = watershed(D2);
bw3 = bw;
bw3(Ld2 == 0) = 0;
figure,imshow(bw3)

###################################################################



url = "/home/marcos/DRIVE/ESTUDOS/UNIOESTE/PDI/projeto-1/OQUKj.jpg";
A = imread(url);
figure, imshow(A);

B = rgb2gray(A);
figure, imshow(B);

C = im2bw(B,0.7);
figure, imshow(C);

D = (A-B)+B;
figure, imshow(D);

E = (A+B)-B;
figure, imshow(E);

F = D-E;
figure, imshow(F);

