close all
clear h

graphics_toolkit qt

#Figura
clear all;
f = figure(...
  'position', [100 50 700 700]
  );

ImgFrm = axes ( ...
  'position', [0, 0.38, 0.45, 0.6], ... 
  'xtick',    [], ... 
  'ytick',    [], ...
  'xlim',     [0, 1], ... 
  'ylim',     [0, 1]
  );

h.p = uipanel ("title", "Panel Title",
               "position", [.46 .38 .535 .61],
               "visible","on");  
h.b15 = uicontrol ("parent",h.p, "style", "pushbutton",
                   "string", "Contador",
                   "Position", [520 5 200 60],
                   "backgroundcolor",[1 1 1],
                   "callback", {@contarObj, ImgFrm});               
###############################################################
h.p1 = uipanel ("parent",h.p,"title", "Object Polarity",
                "position", [.005 .011 .3 .15],
                "visible","on",
                "shadowcolor",[1 1 1]); 
h.p1_radiobuttonDark = uicontrol ("parent",h.p1, "style", "radiobutton",
                             "units", "normalized",
                             "value",1,
                             "string", "Dark",
                             "callback", @botao,
                             "position", [.1 .2 .5 .4]);             
h.p1_radiobuttonBright = uicontrol ("parent",h.p1, "style", "radiobutton",
                             "units", "normalized",
                             "string", "Bright",
                             "value",0,
                             "callback", @botao,
                             "position", [.6 .2 .5 0.4]);
##########################################################################
h.p2 = uipanel ("parent",h.p,"title", "Method",
                "position", [.32 .011 .3 .15],
                "visible","on",
                "shadowcolor",[1 1 1]); 
h.p2_radiobuttonPhse = uicontrol ("parent",h.p2, "style", "radiobutton",
                             "units", "normalized",
                             "string", "Phase-Code",
                             "value",1,
                             "callback", @botao,
                             "position", [.05 .2 .5 .4]);             
h.p2_radiobuttonTwo = uicontrol ("parent",h.p2, "style", "radiobutton",
                             "units", "normalized",
                             "string", "Two-Stage",
                             "callback", @botao,
                             "position", [.55 .2 .5 0.4]);
##########################################################################
h.p3 = uipanel ("parent",h.p,"title", "Method",
                "position", [.01 .65 .98 .3],
                "visible","on",
                "shadowcolor",[1 1 1]); 
h.p3_slidererLabelMin = uicontrol ("parent",h.p3,"style", "text",
                           "units", "normalized",
                           "string", "Raio Min: 15",
                           "horizontalalignment", "left",
                           "visible", "on",
                           "position", [0.01 0.45 0.5 0.5]);                
h.p3_sliderMin = uicontrol ("parent",h.p3,"style", "slider",
                            "units", "normalized",
                            "string", "slider",
                            "callback", @botao,
                            "value", 15,
                            "min", 0.1,
                            "max", 100,
                            "visible", "on",
                            "position", [0.01 0.3 0.4 0.3]);
h.p3_slidererLabelMax = uicontrol ("parent",h.p3,"style", "text",
                           "units", "normalized",
                           "string", "Raio Max: 25",
                           "horizontalalignment", "left",
                           "visible", "on",
                           "position", [0.59 0.45 0.5 0.5]);                             
h.p3_sliderMax = uicontrol ("parent",h.p3,"style", "slider",
                            "units", "normalized",
                            "string", "slider",
                            "callback", @botao,
                            "value", 25,
                            "min", 0.1,
                            "max", 100,
                            "visible", "on",
                            "position", [0.59 0.3 0.4 0.3]);
#####################################################################
h.p4 = uipanel ("parent",h.p,"title", "Sensitivity",
                "position", [.01 .3 .4 .3],
                "visible","on",
                "shadowcolor",[1 1 1]);
h.p4_slidererSenseLabel = uicontrol ("parent",h.p4,"style", "text",
                           "units", "normalized",
                           "string", "Value: 0,75",
                           "horizontalalignment", "left",
                           "visible", "on",
                           "position", [0.05 0.45 0.5 0.5]);                 
h.p4_sliderSense = uicontrol ("parent",h.p4,"style", "slider",
                            "units", "normalized",
                            "string", "slider",
                            "callback", @botao,
                            "value", 0.75,
                            "min", 0,
                            "max", 1,
                            "visible", "on",
                            "position", [0.05 0.3 0.9 0.3]);
#########################################################################
h.p5 = uipanel ("parent",h.p,"title", "Edge Threshold",
                "position", [.59 .3 .4 .3],
                "visible","on",
                "shadowcolor",[1 1 1]);
h.p5_slidererSenseLabel = uicontrol ("parent",h.p5,"style", "text",
                           "units", "normalized",
                           "string", "Value: 0.0",
                           "horizontalalignment", "left",
                           "visible", "on",
                           "position", [0.05 0.45 0.5 0.5]);                 
h.p5_sliderSense = uicontrol ("parent",h.p5,"style", "slider",
                            "units", "normalized",
                            "string", "slider",
                            "callback", @botao,
                            "value", 0.0,
                            "min", 0,
                            "max", 1,
                            "visible", "on",
                            "position", [0.05 0.3 0.9 0.3]);
#######################################################################












                
  


h.Imagem = 0;
h.lista = "none";
#h.slier = 0.5;
h.imghist = 0;

#Botoes

#h.b0 = uicontrol (f, "style", "edit", "string", "", "Position", [10 210 100 36], "callback", {@param});
h.b1 = uicontrol ("parent",f,
                  "style", "pushbutton",
                  "string", "Abrir imagem",
                  "Position", [40 50 100 30],
                  "callback", {@previwImage, ImgFrm});
h.b2 = uicontrol ("parent",f,
                  "style", "pushbutton",
                  "string", "Histograma",
                  "Position", [150 50 100 30],
                  "callback", {@histogramaEscalaCinza, ImgFrm});
h.b3 = uicontrol ("parent",f,
                  "style", "pushbutton",
                  "string", "Manter",
                  "Position", [260 50 100 30],
                  "callback", {@manterAlteracoes, ImgFrm});
h.b4 = uicontrol ("parent",f,
                  "style", "pushbutton",
                  "string", "Salvar",
                  "Position", [260 10 100 30],
                  "callback", {@salvarImagem, ImgFrm});
h.b5 = uicontrol ("parent",f,
                  "style", "pushbutton",
                  "string", "Segment",
                  "Position", [150 10 100 30],
                  "callback", {@tracede, ImgFrm});

###############################################################
#Abri Menu de contagem

h.b6 = uicontrol ("parent",f,
                  "style", "pushbutton",
                  "string", "Segment",
                  "Position", [150 10 100 30],
                  "callback", {@tracede, ImgFrm});


###############################################################


h.b21 = uicontrol ("style", "text",
                                 "units", "normalized",
                                 "string", "Marker style:",
                                 "horizontalalignment", "left",
                                 "position", [0.70 0.3 0.2 0.08]);

h.b22 = uicontrol ("style", "listbox",
                                "units", "normalized",
                                "string", {"Threshold",
                                           "Gray Scale",
                                           "Smooth Img",
                                           "Gradiente Img",
                                           "Strel",
                                           "Normalizar",
                                           "Hisograma Adaptativo"
                                           "Inverter"
                                           "LimpImg"
                                           "Global Threshold"
                                           "Prewitt",
                                           "Passa Alta",
                                           "Passa Alto Reforço",
                                           "Passa Baixa",
                                           "Passa baixa Mediana",
                                           "WaterShed",
                                           "Salt & Papper",
                                           "Gaussian",
                                           "Speckle",
                                           "Poisson",
                                           "Canny",
                                           "Kirsch",
                                           "Lindeberg",
                                           "LoG",
                                           "Roberts",                                                                                      
                                           "Sobel",
                                           "Zerocross",
                                           "Andy"},
                                "callback", @update_plot,
                                "position", [0.70 0.04 0.20 0.26]);
                                

h.b24 = uicontrol ("style", "text",
                                 "units", "normalized",
                                 "string", "Marker marcos:",
                                 "horizontalalignment", "left",
                                 "visible", "off",
                                 "position", [0.45 0.3 0.20 0.08]);                               

h.b23 = uicontrol ("style", "listbox",
                                "units", "normalized",
                                "string", {"None",
                                           "Average",
                                           "Disk",                                           
                                           "Log",
                                           "Gaussian",
                                           "Laplacian",
                                           "Unsharp",
                                           "Motion",
                                           "Sobel",
                                           "Prewitt",
                                           "Kirsch"},
                                "visible", "off",           
                                "callback", @update_plot,
                                "position", [0.45 0.04 0.20 0.26]);

h.b26 = uicontrol ("style", "text",
                                 "units", "normalized",
                                 "string", "Marker marcos:",
                                 "horizontalalignment", "left",
                                 "visible", "off",
                                 "position", [0.45 0.3 0.20 0.08]);                               

h.b25 = uicontrol ("style", "listbox",
                                "units", "normalized",
                                "string", {"Arbitrary",
                                           "Ball",
                                           "Cube",                                           
                                           "Diamond",
                                           "Disk",
                                           "Hypercube",
                                           "Hyperrectangle",
                                           "Line",
                                           "Octagon",
                                           "Pair",
                                           "Periodicline",
                                           "Rectangle",
                                           "Square"},
                                "visible", "off",           
                                "callback", @update_plot,
                                "position", [0.45 0.04 0.20 0.26]); 

h.b28 = uicontrol ("style", "text",
                                 "units", "normalized",
                                 "string", "Suavisação de imagem:",
                                 "horizontalalignment", "left",
                                 "visible", "off",
                                 "position", [0.45 0.3 0.20 0.08]); 
                                
h.b27 = uicontrol ("style", "listbox",
                                "units", "normalized",
                                "string", {"Gaussian",
                                           "Average",
                                           "Disk",                                           
                                           "Median",
                                           "Bilateral",
                                           "P&M",
                                           "Custom Gaussian"},
                                "visible", "off",           
                                "callback", @update_plot,
                                "position", [0.45 0.04 0.20 0.26]); 
#####################################################################
#Global threshold

h.b30 = uicontrol ("style", "text",
                                 "units", "normalized",
                                 "string", "Blobal Threshold:",
                                 "horizontalalignment", "left",
                                 "visible", "off",
                                 "position", [0.45 0.3 0.20 0.08]); 
                                
h.b29 = uicontrol ("style", "listbox",
                                "units", "normalized",
                                "string", {"Otsu",
                                           "concavity",
                                           "intermodes",                                           
                                           "intermeans",
                                           "MaxEntropy",
                                           "MaxLikelihood",
                                           "mean",
                                           "MinError",
                                           "minimum",
                                           "moments",
                                           "percentile"},
                                "visible", "off",           
                                "callback", @update_plot,
                                "position", [0.45 0.04 0.20 0.26]); 
#####################################################################                                
## noise
h.slidererLabel = uicontrol ("style", "text",
                           "units", "normalized",
                           "string", "Noise:",
                           "horizontalalignment", "left",
                           "visible", "off",
                           "position", [0.05 0.3 0.09 0.05]);

h.sliderValue = uicontrol ("style", "slider",
                            "units", "normalized",
                            "string", "slider",
                            "callback", @desliza,
                            "value", 0.5,
                            "min", 0,
                            "max", 1,
                            "visible", "off",
                            "position", [0.03 0.25 0.35 0.06]);

## noise
h.slidererLabel2 = uicontrol ("style", "text",
                           "units", "normalized",
                           "string", "Noise:",
                           "horizontalalignment", "left",
                           "visible", "off",
                           "position", [0.05 0.2 0.09 0.05]);

h.sliderValue2 = uicontrol ("style", "slider",
                            "units", "normalized",
                            "string", "slider",
                            "callback", @desliza,
                            "value", 0.5,
                            "min", 0,
                            "max", 1,
                            "visible", "off",
                            "position", [0.03 0.15 0.35 0.06]);                             

## linestyle
h.linestyle_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Linestyle:",
                               "horizontalalignment", "left",
                               "visible", "off",
                               "position", [0.05 0.3 0.35 0.08]);

h.linestyle_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", {"6",
                                          "18",
                                          "26",
                                          "4",
                                          "8"},
                               "callback", @desliza,
                               "visible", "off",
                               "position", [0.05 0.25 0.35 0.06]);                            
                          
set (gcf, "color", get(0, "defaultuicontrolbackgroundcolor"))
guidata (gcf, h)
#update_plot (gcf, true); 
#endfunction




function atualiza(hObject)
   data = guidata(hObject);
   vari1 = get(data.b22, "string") {get (data.b22, "value")};
   switch(vari1)
    case "Threshold"      
      threshold (hObject);
    case "Gray Scale"    
        greyScale(hObject);
    case "Passa Alta" 
        passaAlta(hObject);
    case "Passa Alto Reforço"  
        passaAltaReforco(hObject);
    case "Hisograma"
        histogramaEscalaCinza(hObject); 
    case "WaterShed"
      waterShed(hObject);
    case "Salt & Papper"
      saltAndPapper(hObject);
    case "Gaussian"
      gaussian(hObject);
    case "Speckle"
      speckle(hObject);
    case "Canny"
      canny(hObject);
    case "Kirsch"
      kirsch(hObject);
    case "Lindeberg"
      lindeberg(hObject);
    case "LoG"
      loG(hObject);
    case "Roberts"
      roberts(hObject);
    case "Zerocross"
      zerocross(hObject);
    case "Andy"
      andy(hObject);
    case "Passa Baixa"
      passaBaixa(hObject);
    case "Normalizar"
      normalizar(hObject);
    case "Strel"
      funStrel(hObject);
    case "LimpImg"
      limpImg(hObject);
    case "Hisograma Adaptativo"
      histAdaptative(hObject);
    case "Smooth Img"
      smoothImg(hObject);
     endswitch

endfunction 

function update_plot(hObject)
  data = guidata(hObject);
  var1 = get(data.b22, "string") {get (data.b22, "value")};
  var2 = get(data.b23, "string") {get (data.b23, "value")};
  var3 = get(data.b25, "string") {get (data.b25, "value")};
  var4 = get(data.b27, "string") {get (data.b27, "value")};
  data.lista = var1;
  #printf("%s %s", var1,var2)
  guidata(hObject, data);
  #atualiza(hObject);  
  
  switch(var1)
    case "Threshold"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.5);
       #printf("teste")     
       threshold (hObject);
    case "LimpImg"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 200);
       set(data.sliderValue, "value", 50);
       #printf("teste")     
       limpImg (hObject);
    case "Hisograma Adaptativo"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 1);
       set(data.sliderValue, "max", 200);
       set(data.sliderValue, "value", 64);        
       histAdaptative(hObject);
    case "Gradiente Img"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.5);        
       imgGradient(hObject);
    case "Inverter"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.5);        
       inverter(hObject);
    case "Gray Scale"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #printf("teste");
       greyScale(hObject);
    case "Passa Alta" 
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       passaAlta(hObject);
    case "Passa Alto Reforço" 
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");      
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 1);
       set(data.sliderValue, "max", 2);
       set(data.sliderValue, "value", 1.5);
       passaAltaReforco(hObject);
    case "WaterShed"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "on");
       set(data.linestyle_popup, "visible", "on");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 4);
       #set(data.sliderValue, "max", 26);
       set(data.sliderValue, "value", 6);
       waterShed(hObject);
    case "Salt & Papper"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.05);
       saltAndPapper(hObject);
    case "Gaussian"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.05);
       gaussian(hObject);
    case "Speckle"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off"); 
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.04);
       speckle(hObject);
    case "Poisson"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.04);
       poisson(hObject); 
    case "Canny"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.4);
       canny(hObject); 
    case "Kirsch"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.05);
       kirsch(hObject); 
    case "Lindeberg"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 10);
       set(data.sliderValue, "value", 2);
       lindeberg(hObject); 
    case "LoG"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.5);
       loG(hObject);
    case "Prewitt"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.05);
       prewitt(hObject);
    case "Roberts"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 10);
       set(data.sliderValue, "value", 1.5);
       roberts(hObject);
    case "Sobel"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.05);
       sobel(hObject); 
    case "Zerocross"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 100);
       set(data.sliderValue, "value", 50);
       zerocross(hObject);
    case "Andy"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 100);
       set(data.sliderValue, "value", 30);
       andy(hObject); 
    case "Passa baixa Mediana"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.04);
       passaBaixaMediana(hObject); 
    case "Passa Baixa" 
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "on");
       set(data.b24, "visible", "on");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       switch (var2)
         case "None"           
           set(data.sliderValue, "visible", "off");
           set(data.slidererLabel, "visible", "off");
           #set(data.sliderValue, "min", 0);
           #set(data.sliderValue, "max", 10);
           #set(data.sliderValue, "value", 3);
         case "Average"           
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 0);
           set(data.sliderValue, "max", 10);
           set(data.sliderValue, "value", 3);
         case "Disk"
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 0);
           set(data.sliderValue, "max", 30);
           set(data.sliderValue, "value", 5);
         case "Gaussian"
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 1);
           set(data.sliderValue, "max", 10);
           set(data.sliderValue, "value", 2);
         case "Log"
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 1);
           set(data.sliderValue, "max", 10);
           set(data.sliderValue, "value", 3);
        case "Laplacian"   
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 0);
           set(data.sliderValue, "max", 1);
           #set(data.sliderValue, "sliderstep", 0.1);
           set(data.sliderValue, "value", 0.2); 
           #passaBaixa(hObject); 
        case "Unsharp"   
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 0);
           set(data.sliderValue, "max", 1);
           set(data.sliderValue, "value", 0.2);         
        case "Motion"   
           set(data.sliderValue, "visible", "on");
           set(data.slidererLabel, "visible", "on");
           set(data.sliderValue, "min", 1);
           set(data.sliderValue, "max", 50);
           set(data.sliderValue, "value", 10);
        case "Sobel"   
           set(data.sliderValue, "visible", "off");
           set(data.slidererLabel, "visible", "off");
           #set(data.sliderValue, "min", 0);
           #set(data.sliderValue, "max", 100);
           #set(data.sliderValue, "value", 9);
         case "Prewitt"   
           set(data.sliderValue, "visible", "off");
           set(data.slidererLabel, "visible", "off");
           #set(data.sliderValue, "min", 0);
           #set(data.sliderValue, "max", 100);
           #set(data.sliderValue, "value", 9);       
       endswitch
       passaBaixa(hObject); 
     case "Global Threshold" 
       set(data.b29, "visible", "on");
       set(data.b30, "visible", "on");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "off");
       set(data.b25, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       set(data.slidererLabel, "visible", "off");
       globalThreshold(hObject);
     case "Normalizar"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "on");
       set(data.b25, "visible", "on");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue, "visible", "on");
       set(data.slidererLabel, "visible", "on");
       set(data.sliderValue, "min", 0);
       set(data.sliderValue, "max", 1);
       set(data.sliderValue, "value", 0.2);          
       switch(var3)
          case "Arbitrary"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 1);
            set(data.sliderValue2, "value", 0); 
          case "Ball"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 10);
          case "Cube"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 10);
          case "Diamond"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 10);
          case "Disk"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 10);
          case "Hypercube"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 10);
            set(data.sliderValue2, "value", 2);
          case "Hyperrectangle"
             set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 10);
            set(data.sliderValue2, "value", 2);
          case "Line"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 10);
            set(data.sliderValue2, "value", 2);
          case "Octagon"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 3);
          case "Pair"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 10);
            set(data.sliderValue2, "value", 2);
          case "Periodicline"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 2);
          case "Rectangle"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 1);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 2);
          case "Square"
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 1);
            set(data.sliderValue2, "max", 30);
            set(data.sliderValue2, "value", 3);
       endswitch
       normalizar(hObject);
     case "Strel"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "off");
       set(data.b27, "visible", "off");
       set(data.b26, "visible", "on");
       set(data.b25, "visible", "on");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.2);          
       switch(var3)
          case "Arbitrary"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 1);
            set(data.sliderValue, "value", 0); 
          case "Ball"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 10);
          case "Cube"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 10);
          case "Diamond"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 10);
          case "Disk"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 10);
          case "Hypercube"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 10);
            set(data.sliderValue, "value", 2);
          case "Hyperrectangle"
             set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 10);
            set(data.sliderValue, "value", 2);
          case "Line"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 10);
            set(data.sliderValue, "value", 2);
          case "Octagon"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 3);
          case "Pair"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 10);
            set(data.sliderValue, "value", 2);
          case "Periodicline"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 2);
          case "Rectangle"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 1);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 2);
          case "Square"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 1);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 3);
       endswitch
       funStrel(hObject);
       case "Smooth Img"
       set(data.b29, "visible", "off");
       set(data.b30, "visible", "off");
       set(data.b28, "visible", "on");
       set(data.b27, "visible", "on");
       set(data.b23, "visible", "off");
       set(data.b24, "visible", "off");
       set(data.linestyle_label, "visible", "off");
       set(data.linestyle_popup, "visible", "off");
       set(data.sliderValue2, "visible", "off");
       set(data.slidererLabel2, "visible", "off");
       #set(data.sliderValue, "min", 0);
       #set(data.sliderValue, "max", 1);
       #set(data.sliderValue, "value", 0.2);          
       switch(var4)
          case "Gaussian"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 1);
            set(data.sliderValue, "value", 0.5); 
          case "Average"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 3);
          case "Disk"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 5);
          case "Median"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 3);
          case "Bilateral"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 2);
          case "P&M"
            set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 30);
            set(data.sliderValue, "value", 10);
            set(data.sliderValue2, "visible", "on");
            set(data.slidererLabel2, "visible", "on");
            set(data.sliderValue2, "min", 0);
            set(data.sliderValue2, "max", 1);
            set(data.sliderValue2, "value", 0.25);
          case "Custom Gaussian"
             set(data.sliderValue, "visible", "on");
            set(data.slidererLabel, "visible", "on");
            set(data.sliderValue, "min", 0);
            set(data.sliderValue, "max", 10);
            set(data.sliderValue, "value", 2);
       endswitch
       smoothImg(hObject);
    endswitch  
  
endfunction

function [z] = mean (x)

  z = x*x;

  

%!test
%!assert (mean(1),1) ;
%!error mean()
%!error (mean(1,1,1))
%!error mean('1as')
%!error mean(1,[1 2 3 4])
%!fail (mean(1,1))



endfunction





function botao(hObject)
  data = guidata(hObject);
  
  switch(gcbo)
    case{data.p1_radiobuttonBright}
      set (data.p1_radiobuttonDark, "value", 0);
    case{data.p1_radiobuttonDark}
      set (data.p1_radiobuttonBright, "value", 0);
    case{data.p2_radiobuttonPhse}
      set (data.p2_radiobuttonTwo, "value", 0);
    case{data.p2_radiobuttonTwo}
      set (data.p2_radiobuttonPhse, "value", 0);
    case{data.p3_sliderMin}
      min = get(data.p3_sliderMin, "value");
      set(data.p3_slidererLabelMin, "string", 
          sprintf ("Raio Min: %.2f", min));
    case{data.p3_sliderMax}
      max = get(data.p3_sliderMax, "value");
      set(data.p3_slidererLabelMax, "string", 
          sprintf ("Raio Max: %.2f", max));
    case{data.p4_sliderSense} 
      sense = get(data.p4_sliderSense, "value");    
      set(data.p4_slidererSenseLabel, "string",
           sprintf ("Value: %.2f", sense)); 
    case{data.p5_sliderSense}
      EdgeThreshold = get(data.p5_sliderSense, "value");    
      set(data.p5_slidererSenseLabel, "string",
           sprintf ("Value: %.2f", EdgeThreshold)); 
  endswitch

%!test
%!#assert (botao(),1) ;
%!error botao()
%!error botao(1,1,1)
%!error botao('1as')
%!error botao()
%!#fail (botao()) 
  
endfunction  



function contarObj(hObject)
  data = guidata(hObject);
  min = get(data.p3_sliderMin, "value");
  
  
  max = get(data.p3_sliderMax, "value");
  
  
  sense = get(data.p4_sliderSense, "value");
  
           
  EdgeThreshold = get(data.p5_sliderSense, "value");
  
 
  va1 = get(data.p1_radiobuttonDark, "value");
  
  if(va1==1) vaDark = "dark";
  else vaDark = "bright";
  endif;
  
  va2 = get(data.p2_radiobuttonPhse, "value");
  
  if(va2 == 1) 
    vaMet = "PhaseCode";
    
  else 
    vaMet = "TwoStage";
    #printf("\n\n%d\n\n",va2);
  endif;
  
  
  
  rgb = data.Imagem; 
  #rgb = imread("rice.jpg"); 
  #imshow(rgb);
  #d = imdistline;
  #delete(d);
  #rgb = rgb2gray(rgb);
  #[centers,radii] = imfindcircles(rgb,[20 25],'ObjectPolarity','dark');
  [centers,radii] = imfindcircles(rgb,[min max],
                    "ObjectPolarity",vaDark,
                    "Sensitivity",sense,
                    "EdgeThreshold",EdgeThreshold,
                    "Method",vaMet );
  #[centersBright,radiiBright,metricBright] = imfindcircles(rgb,[20 25], 'ObjectPolarity','bright','Sensitivity',0.92,'EdgeThreshold',0.1);  delete(hBright) hBright = viscircles(centersBright, radiiBright,'Color','b');
  #imshow(rgb), h = viscircles(centers, radii);
  #[centersBright,radiiBright] = imfindcircles(rgb,[20 25], 'ObjectPolarity','bright','Sensitivity',0.92);
  #imshow(rgb),  hBright = viscircles(centersBright, radiiBright,'Color','b');

  #[centersBright,radiiBright,metricBright] = imfindcircles(rgb,[10 25],'ObjectPolarity','bright','Sensitivity',0.85,'EdgeThreshold',0.1)
  #hBright = viscircles(centersBright, radiiBright,'Color','b');
  #delete(hBright);
  imshow(rgb), h = viscircles(centers,radii);
  num = length(centers);
  #num = num + length(centersBright)
  
  message = sprintf('The number of circles is %d', num);
  msgbox(message);
  

%!test
%!#assert (contarObj(),1) ;
%!error contarObj()
%!error contarObj(1,1,1)
%!error contarObj('1as')
%!error contarObj()
%!#fail (contarObj())  
  
endfunction 


#{

function teste(hObject, eventdata, ImageFrame) 
  data = guidata(hObject);
  
  rgb = data.Imagem; 
  #rgb = imread("rice.jpg"); 
  #imshow(rgb);
  #d = imdistline;
  #delete(d);
  #rgb = rgb2gray(rgb);
  #[centers,radii] = imfindcircles(rgb,[20 25],'ObjectPolarity','dark');
  [centers,radii] = imfindcircles(rgb,[10 25],'ObjectPolarity','dark', 'Sensitivity',0.95);
  #[centersBright,radiiBright,metricBright] = imfindcircles(rgb,[20 25], 'ObjectPolarity','bright','Sensitivity',0.92,'EdgeThreshold',0.1);  delete(hBright) hBright = viscircles(centersBright, radiiBright,'Color','b');
  #imshow(rgb), h = viscircles(centers, radii);
  #[centersBright,radiiBright] = imfindcircles(rgb,[20 25], 'ObjectPolarity','bright','Sensitivity',0.92);
  #imshow(rgb),  hBright = viscircles(centersBright, radiiBright,'Color','b');

  [centersBright,radiiBright,metricBright] = imfindcircles(rgb,[10 25],'ObjectPolarity','bright','Sensitivity',0.85,'EdgeThreshold',0.1)
  hBright = viscircles(centersBright, radiiBright,'Color','b');
  delete(hBright);
  imshow(rgb),  hBright = viscircles(centersBright, radiiBright,'Color','b'), h = viscircles(centers,radii);
  num = length(centers);
  #num = num + length(centersBright)
  
  message = sprintf('The number of circles is %d', num);
  msgbox(message);
  

endfunction 

#}

function globalThreshold(hObject) 
  data = guidata(hObject);  
  #va1 = get(data.sliderValue, "value");
  #set(data.slidererLabel, "string", sprintf ("Noise: %.2f", va1));
  #va2 = get(data.sliderValue2, "value");
  va3 = get(data.b29, "string") {get (data.b29, "value")};    
  #set(data.slidererLabel2, "string", sprintf ("Noise: %.2f", va2));  
  img = data.Imagem;
  

  switch(va3)
    case "Otsu"
      img = im2bw(img,graythresh(img,va3));
    case "concavity"
      img = im2bw(img,graythresh (img,va3));
    case "intermodes"
      img = im2bw(img,graythresh (img,va3));
    case "intermeans"
      img = im2bw(img,graythresh (img,va3));
    case "MaxEntropy"
      img = im2bw(img,graythresh (img,va3));
    case "MaxLikelihood"
      img = im2bw(img,graythresh (img,va3));
    case "mean"
      img = im2bw(img,graythresh (img,va3));
    case "MinError"
      img = im2bw(img,graythresh (img,va3));
    case "minimum"
      img = im2bw(img,graythresh (img,va3));
    case "moments"
      img = im2bw(img,graythresh (img,va3));
    case "percentile"
      img = im2bw(img,graythresh (img,va3));
  endswitch
  data.imghist = img;
  imshow(img, []);
  
  guidata(hObject, data);

%!test
%!#assert (globalThreshold(),1) ;
%!error globalThreshold()
%!error globalThreshold(1,1,1)
%!error globalThreshold('1as')
%!error globalThreshold()
%!#fail (globalThreshold())   
endfunction





set (gcf, "color", get(0, "defaultuicontrolbackgroundcolor"))
guidata (gcf, h)
#update_plot (gcf, true); 
